//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    
    public partial class sp_ErradicationReport_Result
    {
        public int id { get; set; }
        public string Finca { get; set; }
        public string Bloque { get; set; }
        public string Cama { get; set; }
        public string Producto { get; set; }
        public string Variedad { get; set; }
        public string SemanaSiembra { get; set; }
        public Nullable<System.DateTime> FechaSiembra { get; set; }
        public Nullable<int> NoPlanSembradas { get; set; }
        public string MotivoErradicacion { get; set; }
        public string SemErradicacion { get; set; }
        public Nullable<System.DateTime> FecErradicacion { get; set; }
        public Nullable<int> PlantasErradicadas { get; set; }
        public Nullable<decimal> PorcErradicado { get; set; }
    }
}
