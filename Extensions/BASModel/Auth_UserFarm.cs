﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public partial class Auth_UserFarm
    {
        public int ID { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Nombre { get; set; }
        public string Code { get; set; }
        public string Abbr { get; set; }
        public int IDOriginsFarms { get; set; }
        public int IDPaises { get; set; }
        public int FlagFarmProduction { get; set; }
        public int IDBAS_NFDivision { get; set; }
        public int IDPaymentTerm { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Direccion1 { get; set; }
        public string Direccion2 { get; set; }
        public string Zip { get; set; }
        public string ContactoDespachos { get; set; }
        public string EMailContactoDespachos { get; set; }
        public decimal CostoHandling { get; set; }
        public int FlagFarmCode { get; set; }
        public string UPCFarmCode { get; set; }
        public string Codigo { get; set; }
        public string NombreQB { get; set; }
        public string CodigoQB { get; set; }
        public int FlagIFURelabelReq { get; set; }
        public int FlagConfirmaWEB { get; set; }
        public int IDFincasCentrosProduccion { get; set; }
        public int IDProductionPlace { get; set; }
        public int FlagActivo { get; set; }
        public int IDGruposFincas { get; set; }
        public int FlagFarmLocal { get; set; }
        public int FlagEmailFirmOrder { get; set; }
    }
}
