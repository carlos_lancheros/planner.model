﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class BlockTrendResultModel
    {
        public string IDUnidadesNegocios_Nombre { get; set; }
        public string IDInvernaderos_Codigo { get; set; }
        public string IDClasesProductos_Nombre { get; set; }
        public string Semana { get; set; }
        public string IDPlagas_Nombre { get; set; }
        public decimal PorcIncidencia { get; set; }
        public decimal PorcSeveridad { get; set; }
        public int umbral { get; set; }

    }
}
