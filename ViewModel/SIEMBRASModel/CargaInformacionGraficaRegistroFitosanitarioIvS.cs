﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class CargaInformacionGraficaRegistroFitosanitarioIvS
    {
        public int ID { get; set; }
        public string Semana { get; set; }
        public string IDPlagas_Nombre { get; set; }
        public string Nombre { get; set; }
        public int Anio { get; set; }
        public decimal Porcentaje { get; set; }
    }
}
