﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planner.Model
{
    public class IncidenceSeverityModel
    {
        public int WeekID { get; set; }
        public int GreeHouseID { get; set; }
        public int BlockID { get; set; }
        public int ProductID { get; set; }
        public int PestID { get; set; }
        public string PlagaIdM { get; set; }
        public int WeekIni { get; set; }
        public int WeekFin { get; set; }
        public int BusinessUnitID { get; set; }
        public int PlantProductID { get; set; }
        public int ReportType { get; set; }
    }
}
