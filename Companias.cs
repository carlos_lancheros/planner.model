//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Companias
    {
        public int ID { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Telefono2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string PaginaWeb { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> FlagActivo { get; set; }
        public string ObservacionesOrdenCompra { get; set; }
        public Nullable<int> Orden { get; set; }
        public Nullable<int> UniversalID { get; set; }
        public string NombreSAFEX { get; set; }
    }
}
