//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    
    public partial class sp_GetEmployees_Result
    {
        public int Codigo { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string FIngreso { get; set; }
        public string Cargo { get; set; }
        public string Companias { get; set; }
        public string UnidadNegocio { get; set; }
        public string CentroOperacion { get; set; }
        public string CentrosCosto { get; set; }
    }
}
