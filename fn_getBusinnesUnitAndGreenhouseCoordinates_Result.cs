//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    
    public partial class fn_getBusinnesUnitAndGreenhouseCoordinates_Result
    {
        public string Name { get; set; }
        public int GreenhouseId { get; set; }
        public int CoordinateOrder { get; set; }
        public decimal CoordinateLatitude { get; set; }
        public decimal CoordinateAltitude { get; set; }
    }
}
