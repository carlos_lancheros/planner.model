//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Spread_PlantProductShareWeek
    {
        public int PlanProductShareId { get; set; }
        public int Week { get; set; }
        public int SteamQuantity { get; set; }
        public int Year { get; set; }
    }
}
