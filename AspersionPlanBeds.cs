//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planner.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class AspersionPlanBeds
    {
        public int Id { get; set; }
        public Nullable<int> AspersionPlanId { get; set; }
        public string BedId { get; set; }
        public string Flag { get; set; }
        public Nullable<int> Usuario { get; set; }
        public Nullable<bool> ActiveFlag { get; set; }
        public Nullable<int> BlockId { get; set; }
    }
}
